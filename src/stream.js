'use strict';

const async = require('async');

const { FTP_PATH } = require('./config');
const log = require('./log');

module.exports = (ftpClient, mysqlConn, name, done) => {
  log.debug(`${name} started`);

  const remoteDir = `${FTP_PATH}/${name}`;
  const remoteProcessedDir = `${FTP_PATH}/PROCESSED/${name}`;
  const parse = require(`./parse/${name}`);

  async.waterfall(
    [
      next => ftpClient.mkdir(remoteProcessedDir, true, next),

      (msg, val, next) => ftpClient.list(remoteDir, next),

      (list, next) => {
        const streamFiles = list.filter(file => file.name.endsWith('.json')).map(file => next => {
          const filePath = `${remoteDir}/${file.name}`;

          ftpClient.get(filePath, (err, stream) => {
            if (err) return next(err);

            let data = '';

            stream.on('data', chunk => (data += chunk)).once('close', () => {
              let json;
              try {
                json = JSON.parse(data);
              } catch (err) {
                return next(new Error(`unable to parse file ${filePath}: ${err.message}`));
              }

              parse(mysqlConn, json, err => {
                if (err) return next(err);

                log.debug(`${filePath} ${json.length} record processed`);

                const processedFilePath = `${remoteProcessedDir}/${file.name}`;
                ftpClient.rename(filePath, processedFilePath, renameErr => {
                  if (renameErr) {
                    log.debug(`error rename filePath:[${filePath}] processedFilePath:[${processedFilePath}]`);
                    ftpClient.delete(processedFilePath, delErr => {
                      log.debug(`deleted and rename again filePath:[${filePath}] processedFilePath:[${processedFilePath}]`);
                      ftpClient.rename(filePath, processedFilePath, renameErr2 => next());
                    });
                  } else {
                    next()
                  }         
                });
                // ftpClient.rename(filePath, processedFilePath, next);
                // ftpClient.delete(filePath, next);
              });
            });
          });
        });

        async.series(streamFiles, next);
      },
    ],
    err => {
      if (err) log.error(err);

      log.debug(`${name} end`);
      done();
    }
  );
};