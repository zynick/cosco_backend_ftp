'use strict';

const path = require('path');
const pkg = require('../package');

const {
  NODE_ENV = 'development',
  ROOT = path.normalize(__dirname + '/../'),
  APP_INFO = pkg.name + ' ' + pkg.version,
  LOG_LEVEL = 'DEBUG',

  FTP_HOST,
  FTP_PORT,
  FTP_USER,
  FTP_PASS,
  FTP_PATH = '/',

  MYSQL_HOST,
  MYSQL_PORT,
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_DB,

  LOCAL_STATIC_PATH,

  DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss',
} = process.env;

const config = {
  NODE_ENV,
  ROOT,
  APP_INFO,
  LOG_LEVEL,

  FTP_HOST,
  FTP_PORT,
  FTP_USER,
  FTP_PASS,
  FTP_PATH,

  MYSQL_HOST,
  MYSQL_PORT,
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_DB,

  LOCAL_STATIC_PATH,

  DATE_FORMAT,
};

module.exports = config;

if (NODE_ENV === 'development') console.log(JSON.stringify(config,null,2));