'use strict';

const async = require('async');
const fs = require('fs');

const log = require('./log');
const {
  FTP_PATH,
  LOCAL_STATIC_PATH
} = require('./config');


module.exports = (ftpClient, done) => {
  const processName = 'invoice pdf';
  log.debug(`${processName} started`);

  const folderName = 'invoice';
  const remoteSrcDir = `${FTP_PATH}/${folderName}`;
  const remoteDstDir = `${FTP_PATH}/PROCESSED/${folderName}`;

  async.waterfall(
    [
      next => ftpClient.mkdir(remoteDstDir, true, next),

      (msg, val, next) => ftpClient.list(remoteSrcDir, next),

      (list, next) => {
        const streamFiles = list
          .filter(file => file.name.toLowerCase().endsWith('.pdf'))
          .map(file => next => {
            const srcPath = `${remoteSrcDir}/${file.name}`;
            const dstPath = `${remoteDstDir}/${file.name}`;

            ftpClient.delete(dstPath, (err) => {
              if (err && err.code !== 550) return next(err); // proceed if error 550 file not found

              ftpClient.get(srcPath, (err, stream) => {
                if (err) return next(err);

                stream.once('close', () => {
                  ftpClient.rename(srcPath, dstPath, next);
                });

                stream.pipe(fs.createWriteStream(`${LOCAL_STATIC_PATH}/${file.name}`));
              });
            });

          });

        async.series(streamFiles, err => {
          if (err) return next(err);
          log.debug(streamFiles.length + ' pdf downloaded');
          next();
        });
      },
    ],
    err => {
      if (err) log.error(err);

      log.debug(`${processName} end`);
      done();
    }
  );
};