'use strict'; // ignore camel case variable from mysql column
/* jshint -W106 */ const async = require('async');
const moment = require('moment');

const { DATE_FORMAT, APP_INFO } = require('../config');
const jsonFundMatch = require('../json/fundMatch');
const jsonFundMatchFund = require('../json/fundMatchFund');
const jsonFundMatchInvoice = require('../json/fundMatchInvoice');

const insertData = (mysqlConn, data, today, done) => {
  let fundMatchId;

  async.series(
    [
      next => {
        const insert = 'INSERT INTO cbs_fund_match SET ?';
        const dbObject = jsonFundMatch.toDbObject(data);
        dbObject.created_date = today;
        dbObject.created_by = APP_INFO;
        mysqlConn.query(insert, dbObject, (err, results) => {
          if (err) return next(err);
          fundMatchId = results.insertId;
          next();
        });
      },
      next => {
        const { funds = [] } = data;

        const instartFmFunds = funds.map(fund => next => {
          const insert = 'INSERT INTO cbs_fund_match_fund SET ?';
          const dbObject = jsonFundMatchFund.toDbObject(fund);
          dbObject.fund_match_id = fundMatchId;
          dbObject.created_date = today;
          dbObject.created_by = APP_INFO;
          mysqlConn.query(insert, dbObject, next);
        });

        async.series(instartFmFunds, next);
      },
      next => {
        const { invoices = [] } = data;

        const instartFmInvoices = invoices.map(invoice => next => {
          const insert = 'INSERT INTO cbs_fund_match_invoice SET ?';
          const dbObject = jsonFundMatchInvoice.toDbObject(invoice);
          dbObject.fund_match_id = fundMatchId;
          dbObject.created_date = today;
          dbObject.created_by = APP_INFO;
          mysqlConn.query(insert, dbObject, next);
        });

        async.series(instartFmInvoices, next);
      },
    ],
    done
  );
};

const updateData = (mysqlConn, data, today, fundMatchId, done) => {
  async.series(
    [
      next => {
        const update = 'UPDATE cbs_fund_match SET ? WHERE reference_uuid = ?';
        const dbObject = jsonFundMatch.toDbObject(data);
        dbObject.modified_date = today;
        dbObject.modified_by = APP_INFO;
        const updateVal = [dbObject, data.referenceUuid];
        mysqlConn.query(update, updateVal, next);
      },
      next => {
        const remove = 'DELETE FROM cbs_fund_match_fund WHERE fund_match_id = ?';
        const removeVal = [fundMatchId];
        mysqlConn.query(remove, removeVal, next);
      },
      next => {
        const remove = 'DELETE FROM cbs_fund_match_invoice WHERE fund_match_id = ?';
        const removeVal = [fundMatchId];
        mysqlConn.query(remove, removeVal, next);
      },
      next => {
        const { funds = [] } = data;

        const instartFmFunds = funds.map(fund => next => {
          const insert = 'INSERT INTO cbs_fund_match_fund SET ?';
          const dbObject = jsonFundMatchFund.toDbObject(fund);
          dbObject.fund_match_id = fundMatchId;
          dbObject.created_date = today;
          dbObject.created_by = APP_INFO;
          mysqlConn.query(insert, dbObject, next);
        });

        async.series(instartFmFunds, next);
      },
      next => {
        const { invoices = [] } = data;

        const instartFmInvoices = invoices.map(invoice => next => {
          const insert = 'INSERT INTO cbs_fund_match_invoice SET ?';
          const dbObject = jsonFundMatchInvoice.toDbObject(invoice);
          dbObject.fund_match_id = fundMatchId;
          dbObject.created_date = today;
          dbObject.created_by = APP_INFO;
          mysqlConn.query(insert, dbObject, next);
        });

        async.series(instartFmInvoices, next);
      },
    ],
    done
  );
};

module.exports = (mysqlConn, json, done) => {
  const today = new Date();

  async.eachSeries(
    json,
    (data, next) => {
      const select = 'SELECT * FROM cbs_fund_match WHERE reference_uuid = ?';
      const selectVal = [data.referenceUuid];
      mysqlConn.query(select, selectVal, (err, results) => {
        if (err) return next(err);

        // update data if exist, else insert

        if (results.length > 0) {
          // check if update needed
          const oldDate = results[0].update_time;
          const newDate = moment(data.updateTime, DATE_FORMAT);
          if (newDate.isSameOrBefore(oldDate)) return next();

          // update
          const fundMatchId = results[0].id;
          updateData(mysqlConn, data, today, fundMatchId, next);
        } else {
          // insert
          insertData(mysqlConn, data, today, next);
        }
      });
    },
    done
  );
};
