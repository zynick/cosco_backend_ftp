'use strict'; // ignore camel case variable from mysql column
/* jshint -W106 */ const async = require('async');

const { APP_INFO } = require('../config');
const jsonDoc = require('../json/doc');

module.exports = (mysqlConn, json, done) => {
  const today = new Date();

  async.eachSeries(
    json,
    (data, next) => {
      const select = 'SELECT * FROM cbs_doc WHERE business_no = ?';
      const selectVal = [data.businessNo];
      mysqlConn.query(select, selectVal, (err, results) => {
        if (err) return next(err);

        // update data if exist, else insert

        if (results.length > 0) {
          // update
          const update = 'UPDATE cbs_doc SET ? WHERE business_no = ?';
          const dbObject = jsonDoc.toDbObject(data);
          dbObject.modified_date = today;
          dbObject.modified_by = APP_INFO;
          const updateVal = [dbObject, data.businessNo];
          mysqlConn.query(update, updateVal, next);
        } else {
          // insert
          const insert = 'INSERT INTO cbs_doc SET ?';
          const dbObject = jsonDoc.toDbObject(data);
          dbObject.created_date = today;
          dbObject.created_by = APP_INFO;
          mysqlConn.query(insert, dbObject, next);
        }
      });
    },
    done
  );
};
