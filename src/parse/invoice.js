'use strict'; // ignore camel case variable from mysql column
/* jshint -W106 */ const async = require('async');
const moment = require('moment');

const { DATE_FORMAT, APP_INFO } = require('../config');
const jsonInvoice = require('../json/invoice');

module.exports = (mysqlConn, json, done) => {
  const today = new Date();

  async.eachSeries(
    json,
    (data, next) => {
      const select = 'SELECT * FROM cbs_invoice WHERE invoice_no = ?';
      const selectVal = [data.invoiceNo];
      mysqlConn.query(select, selectVal, (err, results) => {
        if (err) return next(err);

        // update data if exist, else insert

        if (results.length > 0) {
          // check if update needed
          const oldDate = results[0].update_time;
          const newDate = moment(data.updateTime, DATE_FORMAT);
          if (newDate.isSameOrBefore(oldDate)) return next();

          // update
          const update = 'UPDATE cbs_invoice SET ? WHERE invoice_no = ?';
          const dbObject = jsonInvoice.toDbObject(data);
          dbObject.modified_date = today;
          dbObject.modified_by = APP_INFO;
          const updateVal = [dbObject, data.invoiceNo];
          mysqlConn.query(update, updateVal, next);
        } else {
          // insert
          const insert = 'INSERT INTO cbs_invoice SET ?';
          const dbObject = jsonInvoice.toDbObject(data);
          dbObject.created_date = today;
          dbObject.created_by = APP_INFO;
          mysqlConn.query(insert, dbObject, next);
        }
      });
    },
    done
  );
};
