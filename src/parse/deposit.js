'use strict'; // ignore camel case variable from mysql column
/* jshint -W106 */ const async = require('async');

const { APP_INFO } = require('../config');
const jsonDeposit = require('../json/deposit');

module.exports = (mysqlConn, json, done) => {
  const today = new Date();

  async.eachSeries(
    json,
    (data, next) => {
      const select = 'SELECT * FROM cbs_deposit WHERE deposit_no = ?';
      const selectVal = [data.depositNo];
      mysqlConn.query(select, selectVal, (err, results) => {
        if (err) return next(err);

        // update data if exist, else insert

        if (results.length > 0) {
          // update
          const update = 'UPDATE cbs_deposit SET ? WHERE deposit_no = ?';
          const dbObject = jsonDeposit.toDbObject(data);
          dbObject.modified_date = today;
          dbObject.modified_by = APP_INFO;
          const updateVal = [dbObject, data.depositNo];
          mysqlConn.query(update, updateVal, next);
        } else {
          // insert
          const insert = 'INSERT INTO cbs_deposit SET ?';
          const dbObject = jsonDeposit.toDbObject(data);
          dbObject.created_date = today;
          dbObject.created_by = APP_INFO;
          mysqlConn.query(insert, dbObject, next);
        }
      });
    },
    done
  );
};
