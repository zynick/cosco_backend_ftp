'use strict'; // ignore camel case variable from mysql column
/* jshint -W106 */ const async = require('async');
const moment = require('moment');

const { DATE_FORMAT, APP_INFO } = require('../config');
const jsonFund = require('../json/fund');

module.exports = (mysqlConn, json, done) => {
  const today = new Date();

  async.eachSeries(
    json,
    (data, next) => {
      const select = 'SELECT * FROM cbs_fund WHERE business_reference_code = ?';
      const selectVal = [data.businessReferenceCode];
      mysqlConn.query(select, selectVal, (err, results) => {
        if (err) return next(err);

        // update data if exist, else insert

        if (results.length > 0) {
          // check if update needed
          const oldDate = results[0].update_time;
          const newDate = moment(data.updateTime, DATE_FORMAT);
          if (newDate.isSameOrBefore(oldDate)) return next();

          // update
          const update = 'UPDATE cbs_fund SET ? WHERE business_reference_code = ?';
          const dbObject = jsonFund.toDbObject(data);
          dbObject.modified_date = today;
          dbObject.modified_by = APP_INFO;
          const updateVal = [dbObject, data.businessReferenceCode];
          mysqlConn.query(update, updateVal, next);
        } else {
          // insert
          const insert = 'INSERT INTO cbs_fund SET ?';
          const dbObject = jsonFund.toDbObject(data);
          dbObject.created_date = today;
          dbObject.created_by = APP_INFO;
          mysqlConn.query(insert, dbObject, next);
        }
      });
    },
    done
  );
};
