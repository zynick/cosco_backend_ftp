'use strict';

/* jshint -W106 */
/* ignore camel case variable from mysql column */

module.exports = {
  toDbObject: json => {
    return {
      amount: json.amount !== '' ? json.amount : null,
      balance: json.balance !== '' ? json.balance : null,
      cheque_date: json.chequeDate !== '' ? json.chequeDate : null,
      cheque_no: json.chequeNo !== '' ? json.chequeNo : null,
      cheque_payer: json.chequePayer !== '' ? json.chequePayer : null,
      del_ind: json.delInd !== '' ? json.delInd : null,
      deposit_no: json.depositNo !== '' ? json.depositNo : null, // ref to deposit.deposit_no
      receipt_no: json.receiptNo !== '' ? json.receiptNo : null,
      reference_uuid: json.referenceUuid !== '' ? json.referenceUuid : null, // id
      regist_time: json.registTime !== '' ? json.registTime : null,
      status: json.status !== '' ? json.status : null,
      update_time: json.updateTime !== '' ? json.updateTime : null,
      update_user: json.updateUser !== '' ? json.updateUser : null,
    };
  },
};
