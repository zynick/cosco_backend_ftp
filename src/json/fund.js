'use strict';
/* jshint -W106 */ // ignore camel case variable from mysql column

module.exports = {

  toDbObject: json => {

    return {
      amount: json.amount !== '' ? json.amount : null,
      balance: json.balance !== '' ? json.balance : null,
      bank_in_date: json.bankInDate !== '' ? json.bankInDate : null,
      business_reference_code: json.businessReferenceCode !== '' ? json.businessReferenceCode :
        null, // id
      collection_register_date: json.collectionRegisterDate !== '' ? json.collectionRegisterDate :
        null,
      collection_type: json.collectionType !== '' ? json.collectionType : null,
      currency_code: json.currencyCode !== '' ? json.currencyCode : null,
      cust_sap_id: json.custSapId !== '' ? json.custSapId : null, // company sap id
      fund_status: json.fundStatus !== '' ? json.fundStatus : null,
      matched_amount: json.matchedAmount !== '' ? json.matchedAmount : null,
      payer_name: json.payerName !== '' ? json.payerName : null,
      remark: json.remark !== '' ? json.remark : null,
      update_time: json.updateTime !== '' ? json.updateTime : null
    };

  }

};