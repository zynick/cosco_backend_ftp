'use strict';
/* jshint -W106 */ // ignore camel case variable from mysql column

module.exports = {

  toDbObject: json => {

    return {
      ccy_code: json.ccyCode !== '' ? json.ccyCode : null,
      claim_amt: json.claimAmt !== '' ? json.claimAmt : null,
      invoice_no: json.invoiceNo !== '' ? json.invoiceNo : null // id - ref to invoice.invoice_no
    };

  }

};