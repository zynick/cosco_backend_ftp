'use strict';

/* jshint -W106 */ // ignore camel case variable from mysql column

module.exports = {

  toDbObject: json => {

    return {
      biz_ref_cde: json.bizRefCde !== '' ? json.bizRefCde : null, // blno - ref to doc.business_no
      business_date: json.businessDate !== '' ? json.businessDate : null,
      ccy_code: json.ccyCode !== '' ? json.ccyCode : null,
      confirm_time: json.confirmTime !== '' ? json.confirmTime : null,
      cust_sap_id: json.custSapId !== '' ? json.custSapId : null, // company sap id
      customer_name: json.customerName !== '' ? json.customerName : null,
      due_date: json.dueDate !== '' ? json.dueDate : null,
      inv_request_num: json.invRequestNum !== '' ? json.invRequestNum : null,
      invoice_amount: json.invoiceAmount !== '' ? json.invoiceAmount : null,
      invoice_balance: json.invoiceBalance !== '' ? json.invoiceBalance : null,
      invoice_no: json.invoiceNo !== '' ? json.invoiceNo : null, // id
      match_status: json.matchStatus !== '' ? json.matchStatus : null,
      settlement_type: json.settlementType !== '' ? json.settlementType : null,
      status: json.status !== '' ? json.status : null,
      update_time: json.updateTime !== '' ? json.updateTime : null
    };

  }

};