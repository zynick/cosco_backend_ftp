'use strict';
/* jshint -W106 */ // ignore camel case variable from mysql column

module.exports = {

  toDbObject: json => {

    return {
      biz_ref_code: json.bizRefCode !== '' ? json.bizRefCode : null, // blno - ref to doc.business_no
      cheque_no: json.chequeNo !== '' ? json.chequeNo : null,
      cheque_payer: json.chequePayer !== '' ? json.chequePayer : null,
      customer: json.customer !== '' ? json.customer : null,
      del_ind: json.delInd !== '' ? json.delInd : null,
      deposit_amount: json.depositAmount !== '' ? json.depositAmount : null,
      deposit_no: json.depositNo !== '' ? json.depositNo : null, // id
      deposit_required: json.depositRequired !== '' ? json.depositRequired : null,
      fund_amount: json.fundAmount !== '' ? json.fundAmount : null,
      operat_time: json.operatTime !== '' ? json.operatTime : null,
      operat_user: json.operatUser !== '' ? json.operatUser : null,
      receipt_no: json.receiptNo !== '' ? json.receiptNo : null,
      status: json.status !== '' ? json.status : null
    };

  }

};