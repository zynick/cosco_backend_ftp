'use strict';
/* jshint -W106 */ // ignore camel case variable from mysql column

module.exports = {

  toDbObject: json => {

    return {
      approval_status: json.approvalStatus !== '' ? json.approvalStatus : null,
      bl_overdue_outs: json.blOverdueOuts !== '' ? json.blOverdueOuts : null,
      bl_total_amount: json.blTotalAmount !== '' ? json.blTotalAmount : null,
      business_date: json.businessDate !== '' ? json.businessDate : null,
      business_no: json.businessNo !== '' ? json.businessNo : null, // id - blno
      create_date: json.createDate !== '' ? json.createDate : null,
      customer_name: json.customerName !== '' ? json.customerName : null,
      deposit_oper: json.depositOper !== '' ? json.depositOper : null,
      deposit_required: json.depositRequired !== '' ? json.depositRequired : null,
      first_port: json.firstPort !== '' ? json.firstPort : null,
      history_outs: json.historyOuts !== '' ? json.historyOuts : null,
      ib_ob: json.ibOb !== '' ? json.ibOb : null,
      issue_status: json.issueStatus !== '' ? json.issueStatus : null,
      issue_user: json.issueUser !== '' ? json.issueUser : null,
      issuer_date: json.issuerDate !== '' ? json.issuerDate : null,
      last_port: json.lastPort !== '' ? json.lastPort : null,
      overdue_days: json.overdueDays !== '' ? json.overdueDays : null,
      requesst_no: json.requesstNo !== '' ? json.requesstNo : null,
      svvd: json.svvd !== '' ? json.svvd : null,
      vessel_name: json.vesselName !== '' ? json.vesselName : null
    };

  }

};