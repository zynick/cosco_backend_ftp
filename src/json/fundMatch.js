'use strict';
/* jshint -W106 */ // ignore camel case variable from mysql column

module.exports = {

  toDbObject: json => {

    return {
      amount: json.amount !== '' ? json.amount : null,
      currency_code: json.currencyCode !== '' ? json.currencyCode : null,
      operat_time: json.operatTime !== '' ? json.operatTime : null,
      operat_user: json.operatUser !== '' ? json.operatUser : null,
      reference_code: json.referenceCode !== '' ? json.referenceCode : null,
      reference_uuid: json.referenceUuid !== '' ? json.referenceUuid : null, // id
      settlement_type: json.settlementType !== '' ? json.settlementType : null,
      status: json.status !== '' ? json.status : null,
      update_time: json.updateTime !== '' ? json.updateTime : null
    };

  }

};