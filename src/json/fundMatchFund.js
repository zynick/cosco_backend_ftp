'use strict';
/* jshint -W106 */ // ignore camel case variable from mysql column

module.exports = {

  toDbObject: json => {

    return {
      business_reference_code: json.businessReferenceCode !== '' ? json.businessReferenceCode :
        null, // id - ref to fund.business_reference_code
      claim_amt: json.claimAmt !== '' ? json.claimAmt : null,
      currency_code: json.currencyCode !== '' ? json.currencyCode : null
    };

  }

};