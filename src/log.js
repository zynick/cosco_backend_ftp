'use strict';

const debugPkg = require('debug');

const { LOG_LEVEL = 'DEBUG' } = require('./config');
const error = debugPkg('ERROR');
const info = debugPkg('INFO');
const debug = debugPkg('DEBUG');

const mode =
  LOG_LEVEL === 'ERROR' ? 'ERROR' : LOG_LEVEL === 'INFO' ? 'ERROR,INFO' : 'ERROR,INFO,DEBUG';

debugPkg.enable(mode);

module.exports = {
  error: msg => error(msg),
  info: msg => info(msg),
  debug: msg => debug(msg),
};
