'use strict';

const async = require('async');
const FTP = require('ftp');
const glob = require('glob');
const mysql = require('mysql');

/**
 * 1. process json file: app -> stream -> parse
 * 2. process pdf file: app -> pdf
 */

const {
  FTP_HOST,
  FTP_PORT,
  FTP_USER,
  FTP_PASS,
  MYSQL_HOST,
  MYSQL_PORT,
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_DB,
} = require('./src/config');
const stream = require('./src/stream');
const pdf = require('./src/pdf');
const log = require('./src/log');

let ftpClient, mysqlConn;

async.series(
  [
    next => {
      // get ftp client

      const config = {
        host: FTP_HOST,
        port: FTP_PORT,
        user: FTP_USER,
        password: FTP_PASS,
      };

      ftpClient = new FTP();
      ftpClient
        .on('ready', next)
        .on('error', next)
        .connect(config);
    },
    next => {
      // get mysql connection

      const config = {
        host: MYSQL_HOST,
        port: MYSQL_PORT,
        user: MYSQL_USER,
        password: MYSQL_PASS,
        database: MYSQL_DB,
      };

      mysqlConn = mysql.createConnection(config);
      mysqlConn.connect(next);
    },
    next => {
      // start

      // glob('./src/parse/*.js', (err, files) => {
      //   if (err) return next(err);

      //   const streamProcesses = files.map(file => {
      //     const start = file.lastIndexOf('/') + 1;
      //     const end = file.lastIndexOf('.js');
      //     const name = file.substr(start, end - start);

      //     return next => stream(ftpClient, mysqlConn, name, next);
      //   });

      //   const pdfProcess = next => pdf(ftpClient, next);

      //   streamProcesses.push(pdfProcess);

      //   async.series(streamProcesses, next);
      // });

      const streamProcesses = []
      streamProcesses.push(next => stream(ftpClient, mysqlConn, 'doc', next))
      const pdfProcess = next => pdf(ftpClient, next);
      streamProcesses.push(pdfProcess);
      async.series(streamProcesses, next);
    },
  ],
  err => {
    if (err) log.error(err);

    if (ftpClient) ftpClient.end();
    if (mysqlConn) mysqlConn.end();

    log.info('process completed');
  }
);